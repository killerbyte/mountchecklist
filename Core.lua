local addonName = ...

_G[addonName] = LibStub("AceAddon-3.0"):NewAddon(addonName, "AceConsole-3.0", "AceEvent-3.0", "AceComm-3.0", "AceSerializer-3.0", "AceTimer-3.0", "LibMVC-1.0")

local addon = _G[addonName]

addon.Version = "v0.1"
addon.VersionNum = 0.1

local commPrefix = addonName

BINDING_HEADER_MOUNTCHECKLIST = addonName

local options = { 
	type = "group",
	args = {
		show = {
			type = "execute",
			name = 'show',
			desc = "Shows the UI",
			func = function() MountCheckListFrame:Show() end
		},
		hide = {
			type = "execute",
			name = 'hide',
			desc = "Hides the UI",
			func = function() MountCheckListFrame:Hide() end
		},
		toggle = {
			type = "execute",
			name = 'toggle',
			desc = "Toggles the UI",
			func = function() addon:ToggleUI() end
		},
	},
}

local AddonDB_Defaults = {
	global = {
		Guilds = {
			['*'] = {			-- ["Account.Realm.Name"] 
				hideInTooltip = nil,		-- true if this guild should not be shown in the tooltip counters
			},
		},
		Characters = {
			['*'] = {					-- ["Account.Realm.Name"] 
			},
		},
		Sharing = {
			Clients = {},
			SharedContent = {			-- lists the shared content
				--	["Account.Realm.Name"]  = true means the char is shared,
				--	["Account.Realm.Name.Module"]  = true means the module is shared for that char
			},
			Domains = {
				['*'] = {			-- ["Account.Realm"] 
					lastSharingTimestamp = nil,	-- a date, the last time information from this realm/account was queried and successfully saved.
					lastUpdatedWith = nil,		-- last player with whom the account sharing took place
				},
			},
		},
		unsafeItems = {},
		options = {
			-- ** Summary tab options **
			["UI.Tabs.Instance.CurrentColumn"] = "Name",
			
			-- ** Minimap options **
			["UI.Minimap.ShowIcon"] = true,
			["UI.Minimap.IconAngle"] = 180,
			["UI.Minimap.IconRadius"] = 78,
			
			-- ** Calendar options **
			["UI.Calendar.WarningsEnabled"] = true,
			["UI.Calendar.UseDialogBoxForWarnings"] = false,	-- use a dialog box for warnings (true), or default chat frame (false)
			["UI.Calendar.WeekStartsOnMonday"] = false,

			WarningType1 = "30|15|10|5|4|3|2|1",		-- for profession cooldowns
			WarningType2 = "30|15|10|5|4|3|2|1",		-- for dungeon resets
			WarningType3 = "30|15|10|5|4|3|2|1",		-- for calendar events
			WarningType4 = "30|15|10|5|4|3|2|1",		-- for item timers (like mysterious egg)
			
			-- ** Global options **
			["UI.AHColorCoding"] = true,					-- color coded recipes at the AH
			["UI.VendorColorCoding"] = true,				-- color coded recipes at vendors
			["UI.AccountSharing.IsEnabled"] = false,	-- account sharing communication handler is disabled by default
			
			["UI.Scale"] = 1.0,
			["UI.Transparency"] = 1.0,
			["UI.ClampWindowToScreen"] = false,

		},
	}
}

addon.Colors = {
	white	= "|cFFFFFFFF",
	red = "|cFFFF0000",
	darkred = "|cFFF00000",
	green = "|cFF00FF00",
	orange = "|cFFFF7F00",
	yellow = "|cFFFFFF00",
	gold = "|cFFFFD700",
	teal = "|cFF00FF9A",
	cyan = "|cFF1CFAFE",
	lightBlue = "|cFFB0B0FF",
	battleNetBlue = "|cff82c5ff",
	grey = "|cFF909090",
	
	-- classes
	classMage = "|cFF69CCF0",
	classHunter = "|cFFABD473",
	
	-- recipes
	recipeGrey = "|cFF808080",
	recipeGreen = "|cFF40C040",
	recipeOrange = "|cFFFF8040",
	
	-- rarity : http://wow.gamepedia.com/Quality
	common = "|cFFFFFFFF",
	uncommon = "|cFF1EFF00",
	rare = "|cFF0070DD",
	epic = "|cFFA335EE",
	legendary = "|cFFFF8000",
	heirloom = "|cFFE6CC80",

	Alliance = "|cFF2459FF",
	Horde = "|cFFFF0000"
}

local tabList = {
    "Instance",
    "Raid",
    "World Boss",
}

-- ** LDB Launcher **
LibStub:GetLibrary("LibDataBroker-1.1"):NewDataObject(addonName, {
	type = "launcher",
	icon = "Interface\\Icons\\INV_Mount_Hippogryph_Arcane",
	OnClick = function(clickedframe, button)
		addon:ToggleUI()
	end,
	text = (Broker2FuBar) and addonName or nil,		-- only for fubar,  not for ldb
	label = addonName,
})

function addon:OnInitialize()
    addon.db = LibStub("AceDB-3.0"):New(addonName .. "DB", AddonDB_Defaults)
    LibStub("AceConfig-3.0"):RegisterOptionsTable(addonName, options)
	addon:RegisterChatCommand("mountchecklist", "ChatCommand")
end

function addon:ChatCommand(input)
    addon:ToggleUI()
	-- if not input then
    --     LibStub("AceConfigDialog-3.0"):Open(addonName)
    --     addon:ToggleUI
	-- else
	-- 	LibStub("AceConfigCmd-3.0").HandleCommand(addon, "mountchecklist", input)
	-- end
end

local frameToID = {}
for index, name in ipairs(tabList) do
	frameToID[name] = index
end

local function SafeLoadAddOn(name)
	if not IsAddOnLoaded(name) then
		LoadAddOn(name)
	end
end

local function ShowTab(name)
	local tab = _G[addonName .. name]
	if tab then
		tab:Show()
	end
end

local function HideTab(name)
	local tab = _G[addonName .. name]
	if tab then
		tab:Hide()
	end
end

addon.Tabs = {}

function addon.Tabs:HideAll()
	for _, tabName in pairs(tabList) do
		HideTab(tabName)
	end
end

-- function addon.Tabs:OnClick(index)
-- 	if type(index) == "string" then
-- 		index = frameToID[index]
-- 	end
	
-- 	PanelTemplates_SetTab(_G[addonName.."Frame"], index);
-- 	self:HideAll()
-- 	self.current = index
	
-- 	if index >= 1 and index <= 7 then
-- 		local moduleName = format("%s_%s", addonName, tabList[index])
-- 		SafeLoadAddOn(moduleName)		-- make this part a bit more generic once we'll have more LoD parts
		
-- 		local parentLevel = MountCheckListFrame:GetFrameLevel()
-- 		local childName = format("%sTab%s", addonName, tabList[index])

-- 		local tabFrame = _G[ childName ]
		
-- 		if tabFrame then
-- 			local childLevel = tabFrame:GetFrameLevel()
			
-- 			if childLevel <= parentLevel then	-- if for any reason a child frame has a level lower or equal to its parent, fix it
-- 				tabFrame:SetFrameLevel(parentLevel+1)
-- 			end
-- 		else
-- 			addon:Print(format("%s is disabled.", moduleName))
-- 		end
-- 	end
	
-- 	ShowTab(tabList[index])
-- end

function addon.Tabs:OnClick(index)
	if type(index) == "string" then
		index = frameToID[index]
	end

	PanelTemplates_SetTab(_G[addonName.."Frame"], index);
	self:HideAll()
	self.current = index

	ShowTab(tabList[index])
end

tinsert(UISpecialFrames, "MountCheckListFrame")