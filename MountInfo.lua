_G instanceMounts = {69, 185, 213, 264, 248, 395, 397, 411, 410}; -- All mount id's for instance loot drop mounts
_G raidMounts = {117, 118, 119, 120, 168, 183, 287, 246, 247, 253, 250, 349, 304, 363, 396, 425, 415, 445, 442, 444, 478, 531, 543}; -- All mount id's for raid loot drop mounts
_G worldDropMounts = {517, 473, 515, 542, 533}; -- All mount id's for world boss drop mounts
_G expansion = { vanilla = "Vanilla", bc = "The Burning Crusade", wrath = "Wrath of the Lich King", cata = "Cataclysm", 
                    Pandaria = "Mists of Pandaria", warlords = "Warlords of Draenor", legion = "Legion", bfa = "Battle for Azeroth"}
_G expansionInstanceCount = {1, 2, 2, 4, 0, 0, 0, 0} -- Not counting Amani Battle Bear
_G expansionRaidCount = {4, 2, 8, 6, 4, 0, 0, 0}
_G expansionWorldCount = {0, 0, 0, 0, 5, 0, 0, 0}