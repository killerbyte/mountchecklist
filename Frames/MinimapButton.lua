local addonName = ...
local addon = _G[addonName]
local colors = addon.Colors

local OPTION_ICON_ANGLE = "UI.Minimap.IconAngle"
local OPTION_ICON_RADIUS = "UI.Minimap.IconRadius"

local function GetIconAngle()
	local xPos, yPos = GetCursorPosition()

	xPos = Minimap:GetLeft() - xPos/UIParent:GetScale() + 70 
	yPos = yPos/UIParent:GetScale() - Minimap:GetBottom() - 70 

	local iconAngle = math.deg(math.atan2(yPos, xPos))
	if iconAngle < 0 then
		iconAngle = iconAngle + 360
	end

	print(format("X: %s, Y: %s", xPos, yPos))
	
	return iconAngle
end

function MiniMap_OnBind(frame) 
	frame.tooltip = format("%s\n\n%s%s\n%s%s", addonName, colors.white, "Left-click to |cFF00FF00open", colors.white, "Right-click to |cFF00FF00drag")
end

function MiniMap_Move(frame)
	local angle = addon:GetOption(OPTION_ICON_ANGLE)
	local radius = addon:GetOption(OPTION_ICON_RADIUS)

	frame:SetPoint("TOPLEFT", "Minimap", "TOPLEFT", 54 - (radius * cos(angle)), (radius * sin(angle)) - 55 )
end

function MiniMap_Update(frame)
	if frame.isMoving then
		local iconAngle = GetIconAngle()
		addon:SetOption(OPTION_ICON_ANGLE, iconAngle)
		MiniMap_Move(frame);
	end
end

function MiniMapButton_OnClick(button)
	if button == "LeftButton" then
		addon:ToggleUI()
	end
end

function MiniMap_ShowWidgetTooltip(frame)
	if not frame.tooltip then return end
	
	MountTooltip:SetOwner(frame, "ANCHOR_LEFT");
	MountTooltip:ClearLines();
	MountTooltip:AddLine(frame.tooltip)
	MountTooltip:Show(); 
end

-- addon:Controller("MountCheckListUI.MinimapButton", {
-- 	OnBind = function(frame)
-- 		frame.tooltip = format("%s\n\n%s%s\n%s%s", addonName,	
-- 			colors.white, "Left-click to |cFF00FF00open", 
-- 			colors.white, "Right-click to |cFF00FF00drag")
-- 	end,
-- 	Move = function(frame)
-- 		local angle = addon:GetOption(OPTION_ICON_ANGLE)
-- 		local radius = addon:GetOption(OPTION_ICON_RADIUS)
		
-- 		frame:SetPoint( "TOPLEFT", "Minimap", "TOPLEFT", 54 - (radius * cos(angle)), (radius * sin(angle)) - 55 )
-- 	end,
-- 	Update = function(frame)
-- 		if frame.isMoving then
-- 			local iconAngle = GetIconAngle()
-- 			addon:SetOption(OPTION_ICON_ANGLE, iconAngle)
			
-- 			-- this line should not be here, but stays temporarily
-- 			--AltoholicGeneralOptions_SliderAngle:SetValue(iconAngle)
-- 		end
-- 	end,
-- 	Button_OnClick = function(frame, button)
-- 		if button == "LeftButton" then
-- 			addon:ToggleUI()
-- 		end
-- 	end,
-- })
