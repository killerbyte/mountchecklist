## Interface: 70300
## Version: 0.1
## Title: MountCheckList
## Notes: A addon for keeping track of which mounts you've farmed for the day/week
## Author: killerbyte
## OptionalDeps: Ace3
## X-Embeds: Ace3

#@no-lib-strip@
# Libraries
embeds.xml
#@end-no-lib-strip@

Core.lua
Templates\UITemplates.xml
MountCheckList.xml

Frames\MinimapButton.xml