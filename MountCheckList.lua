local addonName = ...
local addon = _G[addonName]
local colors = addon.Colors

local currentAlt = UnitName("player")
local currentRealm = GetRealmName()
local currentAccount = THIS_ACCOUNT

function addon:OnEnable()
    addon:SetupOptions()

    MountCheckListFrameName:SetText(format("Mount Farm Checklist %s%s by %skillerbyte ", colors.white, addon.Version, colors.classMage))
    MountCheckListFrameTab1:SetText("Instance")
    MountCheckListFrameTab2:SetText("Raid")
    MountCheckListFrameTab3:SetText("World Boss")

    -- if addon:GetOption("UI.Minimap.ShowIcon") then
	-- 	Minimap.MountCheckListButton:Move()
	-- 	Minimap.MountCheckListButton:Show()
	-- else
	-- 	Minimap.MountCheckListButton:Hide()
	-- end
end

function addon:OnDisable()

end

function addon:ToggleUI()
	if (MountCheckListFrame:IsVisible()) then
		MountCheckListFrame:Hide()
	else
		MountCheckListFrame:Show()
	end
end

function MountCheckList_OnShow()
	SetPortraitTexture(MountCheckListFramePortrait, "player")
	
	if not addon.Tabs.current then
		addon.Tabs:OnClick("Instance")
		--local mode = addon:GetOption("UI.Tabs.Summary.CurrentMode")
		--AltoholicTabSummary["MenuItem"..mode]:Item_OnClick()
	end
end

function addon:SetupOptions()
    LibStub("LibAboutPanel").new(addonName, addonName);
end

function addon:GetOption(name)
	if addon.db and addon.db.global and addon.db.global.options then
		return addon.db.global.options[name]
	end
end

function addon:SetOption(name, value)
    print(format("Option Name: %s, Value: %s", name, value))

	if addon.db and addon.db.global and addon.db.global.options then 
		addon.db.global.options[name] = value
	end
end

function addon:ShowWidgetTooltip(frame)
	if not frame.tooltip then return end
	
	MountTooltip:SetOwner(frame, "ANCHOR_LEFT");
	MountTooltip:ClearLines();
	MountTooltip:AddLine(frame.tooltip)
	MountTooltip:Show(); 
end